﻿using System.Collections;
using UnityEngine;

/// <summary>
/// A class for making things jump when you make a fist using the Myo
/// </summary>
public class Jump : MonoBehaviour
{
    // Public objects can be "set" from the Unity3D Editor
    public Rigidbody Cube;
    public ThalmicMyo Myo;
    // Keep track of our jumping state
    public bool isJumping = false;

    // Executes on first launch
    void Start()
    {
        StartCoroutine(MoveCheck());
    }

    // Runs every 1/10th of a second to see if we need to apply force
    IEnumerator MoveCheck()
    {
        while (Application.isPlaying)
        {
            // Adds force when the user makes a fist or presses the enter key
            if (!isJumping && (Myo.pose == Thalmic.Myo.Pose.Fist || Input.GetKey(KeyCode.Return)))
            {
                var dir = Cube.transform.position - Cube.transform.GetChild(0).position * 2;
                Cube.AddForce(dir, ForceMode.Impulse);
                isJumping = true;
            }
            else if (isJumping && (Myo.pose != Thalmic.Myo.Pose.Fist && !Input.GetKey(KeyCode.Return)))
            {
                isJumping = false;
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
}